include <BOSL2/std.scad>
include <BOSL2/gears.scad>
include <BOSL2/metric_screws.scad>

clearance=1;


module mounting_hole_mask(size=5, retention_type="nut",insert_d1,insert_d2,insert_h){
    if(retention=="nut"){
        hull(){
            metric_nut(size=size, hole=false,orient=FRONT);
            fwd(8)
            metric_nut(size=size, hole=false,orient=FRONT);
            }
    }
    if (retention=="insert") {
        cylinder(h=insert_h+0.1, d1=insert_d1, d2=insert_d2,orient=FRONT);
    }
}

module geared_mount(l=65,w=60,t=10,arm_l=30,modulus=1,gear_teeth=40,worm_diam=30,worm_starts=1,detailed=true,rounding=1,retention="nut",shaft_mount="bearing") {
    hub_d=30;
    insert_h=5;
    insert_d1=7;
    insert_d2=5.8;
    shaft_d = 8;
    sleeve_d = 12;
    mount_bolt_size = 5;


    difference(){
        union(){
            worm_gear(mod=modulus, teeth=gear_teeth, worm_diam=worm_diam, worm_starts=worm_starts,orient=RIGHT,clearance=clearance);
            hull(){
            xcyl(d=hub_d,l=39,rounding=1);
            fwd(arm_l)
                cuboid([39,10,36],anchor=FRONT,rounding=1);
            }
            fwd(arm_l){
                cuboid([l,t,w],anchor=FRONT,rounding=rounding);
            }
        }
        if (detailed) {
        if (shaft_mount=="bearing") {
            color("blue")xflip_copy(offset=20) xcyl(d=22,h=7.25,anchor=RIGHT);
            color("blue")xflip_copy(offset=20) xcyl(d=12,h=10,anchor=RIGHT);
            color("silver") xcyl(d=shaft_d+.5,h=100);
        }
        if (shaft_mount=="none") {
            color("silver") xcyl(d=shaft_d+0.2,h=100);
        }
        if (shaft_mount=="sleeve") {
            color("silver") xcyl(d=sleeve_d+0.18,h=100);
        }


        xcopies(l=l-16,n=2){
        fwd(15)ycyl(d=mount_bolt_size+0.2,h=30,anchor=BACK,$fn=32);
        if(retention=="nut"){
                fwd(15)
                hull(){
                     metric_nut(size=5, hole=false,orient=FRONT);
                     fwd(8)
                     metric_nut(size=5, hole=false,orient=FRONT);
                }
            }
            if (retention=="insert") {
                fwd(arm_l-(t+0.1))
                cylinder(h=insert_h+0.1, d1=insert_d1, d2=insert_d2,orient=FRONT);
            }

        }

        xcopies(l=l-16,n=4) zcopies(spacing=40){

            fwd(15)
            ycyl(d=mount_bolt_size+0.2,h=30,anchor=BACK,$fn=32);
            if(retention=="nut"){
                fwd(15)
                hull(){
                     metric_nut(size=5, hole=false,orient=FRONT);
                     fwd(8)
                     metric_nut(size=5, hole=false,orient=FRONT);
                }
            }
            if (retention=="insert") {
                fwd(arm_l-(t+0.1))
                cylinder(h=insert_h+0.1, d1=insert_d1, d2=insert_d2,orient=FRONT);
            }
        }
        }

    }

}



module worm_drive(l=40,worm_l=30,modulus=1,worm_diam=30,worm_starts=1,detailed=true,shaft_d=8,collar=1,setscrew=4,setscrew_n=3) {
    setscrew_size = setscrew;
    spacing = l-(get_metric_socket_cap_diam(setscrew_size)+2);

    difference(){
        union(){
        worm(d=worm_diam, l=worm_l, mod=modulus, starts=worm_starts, orient=UP,clearance=clearance);
            zcyl(d=worm_diam,h=l-(2*collar));
            if(collar>0){
                zcyl(d=10,h=collar);
            }
        }
        if (detailed) {
            zcyl(d=0.15+shaft_d,h=l*2);

            zrot_copies(n=setscrew_n)
            zflip_copy(spacing/2)
            {
            ycyl(d=setscrew+0.25,h=worm_diam,anchor=FRONT);
            // back((worm_diam/2)-get_metric_socket_cap_height(setscrew_size)-1)
            // ycyl(d=get_metric_socket_cap_diam(setscrew_size),h=10,anchor=FRONT,$fn=32);
            back(8)
            hull(){
                metric_nut(size=setscrew_size, hole=false,orient=FRONT);
                up(50)
                metric_nut(size=setscrew_size, hole=false,orient=FRONT);
            }
            }

        }

    }


    // body...
}


module housing(
    shaft_offset,
    mount_l=60,
    worm_d=30,
    worm_shaft_d=8,
    mount_shaft_d=8,
    detailed=true,
    extrusion_slots = 2,
    mount_bolt_size = 4,
    mount_hole_base_t = 4,
    show="all"
    ) {
    arm_h=extrusion_h;
    arm_t = 10;
    drive_clearance= 5;
    shaft_clearance = 0.2;
    arm_w=extrusion_w + (2*arm_t);

    drive_block_od=25;

    tube_l=mount_l+ drive_block_od +drive_clearance;
    // thrust_bearing_od = 19;
    // thrust_bearing_h = 2;
    drive_bearing_od=16;
    drive_bearing_h=5;


    mount_bolt_sp = (drive_clearance)+(worm_d/2)+mount_bolt_size;

    attach_bolt_size = 4;
    attach_x_spacing = arm_w - arm_t;
    attach_y_spacing = mount_l+drive_block_od/2;
    attach_y_sp = drive_block_od/2;


    difference(){

    union(){
        // rect_tube(size=60,isize=extrusion_w+0.2,  chamfer=5, irounding=0, h=tube_l,orient=FRONT,anchor=TOP);
        // // this is just to prevent mounting the worm too close to the extrusion
        // back((drive_clearance-2)+(worm_d/2))
        // rect_tube(size=extrusion_w+5,isize=extrusion_w, irounding=4, h=2,orient=FRONT,anchor=TOP);

if (show=="all" || show== "side") {
            xflip_copy(offset=extrusion_w/2)
            hull(){
                fwd(shaft_offset){
                xcyl(d=35,h=arm_t,anchor=LEFT,chamfer=1);
                // xcyl(d=15,h=15,anchor=LEFT);
                }
                // back(drive_clearance + worm_d/2)
                cuboid([arm_t,tube_l,arm_h],anchor=LEFT+FRONT,chamfer=1);
            }

}
    // brackets for the drive shaft
    if (show=="all" || show== "top") {

        zflip_copy(extrusion_h/2)
        union(){
            hull(){
                zcyl(d=drive_block_od,h=arm_t,anchor=BOTTOM,chamfer=1);
                cuboid([35,tube_l,arm_t],anchor=BOTTOM+FRONT,chamfer=1);
            }
            cuboid([arm_w,tube_l,arm_t-5],anchor=BOTTOM+FRONT,chamfer=1,except=BOTTOM);
        }
    }
// if (show=="all" || show== "bottom") {
//         down(extrusion_h/2)
//         hull(){
//             zcyl(d=drive_block_od,h=arm_t,anchor=TOP);
//             // back(drive_clearance + worm_d/2)
//             cuboid([arm_w,tube_l,arm_t],anchor=TOP+FRONT);
//         }
// }
    }
    if (detailed) {
        // zflip_copy(extrusion_h/2){
        // zcyl(d=thrust_bearing_od+0.4,h=thrust_bearing_h+0.5);
        // }

        zflip_copy(1+arm_t+ extrusion_h/2){
        zcyl(d=drive_bearing_od,h=drive_bearing_h,anchor=TOP);
        }
        zcyl(d=worm_shaft_d+shaft_clearance,h=extrusion_h*2);
        fwd(gear_offset)
        xcyl(d=mount_shaft_d+shaft_clearance,h=extrusion_w*2);

        // Mounting holes
        // Sides
        zcopies(n=extrusion_slots,spacing=20)
        ycopies(spacing=20,l=mount_l,sp=mount_bolt_sp){
        xcyl(d=mount_bolt_size+0.2,h=100);

       xflip_copy(extrusion_w/2){
            right(mount_hole_base_t)
            xcyl(d=mount_bolt_size*2,h=20,anchor=LEFT);
        }

        }

        // Top and Bottom
        xcopies(n=extrusion_slots,spacing=20)
        ycopies(spacing=20,l=mount_l,sp=mount_bolt_sp){
        zcyl(d=mount_bolt_size+0.2,h=100);
        // recessed sockets for bolts
        zflip_copy((arm_h)/2)
        // up(1+arm_t/2)
        up(mount_hole_base_t)
        zcyl(d=mount_bolt_size*2,h=50,anchor=BOTTOM);

        }

        // joints
        echo(str("Edge/joint holes are spaced along the Y axis by 20mm"));
        echo(str("Edge/joint holes are seperated along the X axis by ",attach_x_spacing));

        // notch out the side where the motor mount attaches
        cutout_h=5;
        cutout_y_offset= attach_y_sp-(attach_bolt_size*2);

        zflip_copy((extrusion_h/2)+arm_t)
        xflip_copy(extrusion_w/2){
            down(cutout_h)back(cutout_y_offset)
            cuboid([20,60,cutout_h+1],anchor=FRONT+BOTTOM+LEFT,chamfer=2,edges="Z");
        }
        echo(str("Motor mount cutout length ",60));
        echo(str("Motor mount cutout start distance from drive shaft ",cutout_y_offset ));
        echo(str("Motor mount cutout inset depth ",cutout_h));

        ycopies(l=attach_y_spacing,sp=attach_y_sp,spacing=20){
        xflip_copy(attach_x_spacing/2){

            zflip_copy((extrusion_h+arm_t)/2){
                zcyl(d=attach_bolt_size+0.2,h=arm_t+20);
                up(1+arm_t/2)
                zcyl(d=attach_bolt_size*2+0.5,h=attach_bolt_size,anchor=TOP);
            }
            zflip_copy((arm_h-8)/2)
            hull(){
                metric_nut(size=attach_bolt_size, hole=false,orient=UP);
                right(50)
                metric_nut(size=attach_bolt_size, hole=false,orient=UP);
            }
        }
        }
    }
    }

}

// style can be blank,nema, or round
module motor_mount(
    h=20,
    shaft_offset=15,
    style="blank"

    ) {
    attach_w=50;
    attach_bolt_y_spacing = 20;
    attach_part_w = 10;
    attach_part_l = 80;
    attach_part_h = 5;


    xflip_copy(attach_w/2){
        cuboid([attach_part_w,motor_mount_l,attach_part_h]);

    }

}


module housing_top(
    t=10,
    mount_l=60,
    worm_d=30,
    worm_shaft_d=8,
    mount_shaft_d=8,
    detailed=true,
    flanged_drive_baring = false,
    extrusion_slots = 2,
    mount_bolt_size = 4,
    mount_hole_base_t = 4.5,

    drive_block_od=24,
    arm_t = 12,


    ) {

    housing_chamfer = 1;
    arm_h=extrusion_h;
    drive_clearance= 5;
    shaft_clearance = 0.5;
    arm_w=extrusion_w + (2*arm_t);


    drive_block_h=15;
    drive_block_shift = 4;
    drive_block_diff_h = drive_block_h-t;
    tube_l=mount_l+ drive_block_od +drive_clearance;
    thrust_bearing_od = 19;
    drive_bearing_od=16;
    drive_bearing_h=5;


    mount_bolt_sp = (drive_clearance)+(worm_d/2)+mount_bolt_size;

    attach_bolt_size = 5;
    attach_x_spacing = arm_w - arm_t;
    attach_y_spacing = mount_l+drive_block_od;
    attach_y_sp = drive_block_od;
    attach_hole_socket_h = attach_bolt_size+1;
    // attach_hole_socket_d = (2*attach_bolt_size)+1;
    attach_hole_socket_d = 9.5;
    // attach_y_sp = 0;



    difference(){
        union(){

                // up(t-EPSILON){
                // back(drive_block_shift)
                // prismoid(size1=[drive_block_od,drive_block_od+drive_block_shift*2], size2=[drive_block_od,drive_block_od], h=drive_block_diff_h, shift=[0,-drive_block_shift],anchor=BOTTOM,chamfer=housing_chamfer);
                // prismoid(size1=[arm_w,drive_block_od], size2=[drive_block_od,drive_block_od], h=drive_block_diff_h,anchor=BOTTOM,chamfer=housing_chamfer);

                // }
                hull(){

                up(t-EPSILON)cuboid([drive_block_od,drive_block_od,drive_block_diff_h],anchor=BOTTOM,chamfer=housing_chamfer,except=BOTTOM);
                back(drive_block_shift)cuboid([arm_w,drive_block_od+drive_block_shift*2,t],anchor=BOTTOM,chamfer=housing_chamfer,except=BOTTOM);
                }

                // cuboid([arm_w,drive_block_od,t],anchor=BOTTOM,chamfer=housing_chamfer,except=BOTTOM);
                cuboid([arm_w,tube_l,t],anchor=BOTTOM+FRONT,chamfer=housing_chamfer,except=BOTTOM);
        }

        if (detailed) {

        // Bearing and Shaft
        // if (flanged_drive_baring) {
            // zcyl(d=drive_bearing_od,h=drive_block_h*4);
        // }else{
            up(drive_block_h-drive_bearing_h)
            zcyl(d=drive_bearing_od,h=extrusion_h*2,anchor=BOTTOM);
            zcyl(d=worm_shaft_d+shaft_clearance,h=extrusion_h*2);

        // }


        // Top and Bottom
        xcopies(n=extrusion_slots,spacing=20)
        ycopies(spacing=20,l=mount_l,sp=mount_bolt_sp){
        zcyl(d=mount_bolt_size+0.2,h=100);
        // recessed sockets for bolts

        // up(1+arm_t/2)
        up(mount_hole_base_t)
        zcyl(d=mount_bolt_size*2,h=50,anchor=BOTTOM);

        }

        // joints
        echo(str("Edge/joint holes are spaced along the Y axis by 20mm"));
        echo(str("Edge/joint holes are seperated along the X axis by ",attach_x_spacing));


        cutout_h=5;
        cutout_y_offset= attach_y_sp-(attach_bolt_size*2);
         xflip_copy(attach_x_spacing/2){
                zcyl(d=attach_bolt_size+0.2,h=arm_t+20);
                up(t)
                zcyl(d=attach_hole_socket_d,h=50,anchor=BOTTOM);
        }


        ycopies(l=attach_y_spacing,sp=attach_y_sp,spacing=20){
            xflip_copy(attach_x_spacing/2){
                    zcyl(d=M5+0.2,h=arm_t+20);
                    up(t-attach_hole_socket_h)
                    zcyl(d=attach_hole_socket_d,h=50,anchor=BOTTOM);
            }
        }


        }
    }
}


module housing_side(
    shaft_offset,
    t=12,
    mount_l=60,
    worm_d=30,
    mount_shaft_d=8,
    detailed=true,
    extrusion_slots = 2,
    mount_bolt_size = 4,
    mount_hole_base_t = 4.5,
) {
    // body...
}
