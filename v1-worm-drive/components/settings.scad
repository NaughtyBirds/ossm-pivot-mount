include <BOSL2/std.scad>
include <BOSL2/gears.scad>

// constants for normal bolt sizes
M10 = 10;
M8 = 8;
M6 = 5;
M5 = 5;
M4 = 4;
M3 = 3;
M2 = 2;

// actual shared settings

$fn=72;
starts=1;
mod=2;
gear_teeth=24;
worm_diam = 30;
worm_length = 36;
mount_shaft_type="none";
attach_bolt_size=M4;
motor_mount_l=60;

extrusion_w=40;
extrusion_h=40;



gear_pitch_r = pitch_radius(mod=mod, teeth=gear_teeth);
gear_or= outer_radius(mod=mod,teeth=gear_teeth);
gear_offset = (gear_pitch_r+(worm_diam/2));


echo(str("Worm gear pitch radius = ", gear_pitch_r));
echo(str("Worm gear outer radius = ", gear_or));
echo(str("Worm gear pitch radius = ", gear_pitch_r));
echo(str("Worm and gear shaft offset = ", gear_offset));
