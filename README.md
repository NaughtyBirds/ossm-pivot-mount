# OSSM Stand controllable via Actuators

## Pivoting Mount

> This does **not** include anything to do with vertical movement, this is the pivoting action only.

![pivot mount](img/Pivoting-mount-basics.drawio.png)



## Pivot Attachment Layout

Looking at the pivot mechanism from the side, the important values are defined below.

![points](img/measurements.png)

|  Variable  | Definition                                                                             |
| :--------: | -------------------------------------------------------------------------------------- |
|  **`P`**   | The Primary pivot point                                                                |
|  **`S`**   | The center of the OSSM shaft.                                                          |
|  **`A1`**  | The actuator's _static_ mounting point.                                                |
|  **`A2`**  | The actuator's moving mounting point, attached to the pivoting assembly.               |
| **`ΔPB`**  | The distance from the Pivot point `P` to the Body `B`                                  |
| **`ΔPS`**  | The distance from the Pivot point `P` to the OSSM Motor Shaft `S`                      |
| **`ΔPA2`** | The distance from the Pivot point `P` to the actuator end mount                        |
| **`ΔPA1`** | The difference in height from the static point `A1` to the height of `P`               |
| **`Δes`**  | The effective stroke length ie. the current difference in length from fully retracted. |



**The distance from `P` to `A2` determines the amount of rotation for a given `Δes`.**

It's similar to the radius of a gear, the smaller the gear the more rotation for a given length of belt.


**`ΔPA1` should be as close as possible to `ΔPA2`.**

 If `ΔPA1` is too _low_ the actuator will hit the body at full extension.
 Too _high_ and the pull force at full retraction pulls up instead of back, and will rip apart ... something. Whatever is weakest **will** break.

 > I try to make sure there is at least 1-3cm of clearance at full extension.
 > This can be simplified by having the actuator offset from the Body, but that adds other complications with mounting and interference with the OSSM stroke.


 **If `S` is in line with `P` then `ΔPS` just magnifies the amount of vertical travel the toy will experience from any given rotation.**

> It basically adds to the giant lever that is the OSSM stroke length.


**If `S` is _NOT_ in line with `P` then up and down travel will not be even, one will go further.**
> The system is also MUCH more difficult to reason about, I recommend keeping as much lined up as possible for everyones sanity.







### Tools

- A threaded Tap for the end of the extrusion, OpenBuild's v-slot has holes for a 5mm tap.
- A drill
- 5/16" (8mm) drill bit
- 1/4" (6mm) drill bit

### Purchased Hardware

| Part                                       |             QTY              | Link                                                      | Notes                                                                                                            |
| ------------------------------------------ | :--------------------------: | --------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------- |
| V-slot/T-Slot T-Joining Plate              |              4               | https://openbuildspartstore.com/t-joining-plate/          | Similar ones from amazon should work fine.                                                                       |
| **V-Slot** 40x40                           |          1 x 500mm           | https://openbuildspartstore.com/v-slot-40x40-linear-rail/ | Something else should work, but may need adjustments.                                                            |
| **V-Slot** 20x40                           |       250mm is plenty        | https://openbuildspartstore.com/v-slot-20x40-linear-rail/ | T-Slot will also work.                                                                                           |
| Steel L-Bracket for NEMA 23 Stepper Motors |              1               | https://www.pololu.com/product/2258                       | I used this one, similar should be fine.                                                                         |
| 4in (100mm) 12v actuator                   |              1               | https://www.amazon.com/dp/B0B4B55B9K                      | Retracted 6.3" (160mm) Extended 12.2" (310mm) - the length is important, but reasonable variations should be ok. |
| M6 x50 screws or bolts                     | 2 (seems low? need to check) |                                                           |
| M6 nuts                                    | 2 (seems low? need to check) |                                                           |

## Printed parts

There are two printed parts, an actuator mount and the end mount pivot bock/housing.
