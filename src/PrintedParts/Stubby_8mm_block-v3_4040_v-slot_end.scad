// include <BOSL2/std.scad>
include <constants.scad>

$fn=60;


screw_cap_d = 10;
screw_d=5.25;
shaft_clearence=.1;
shaft_d = 8;
shaft_h=8;

wall = 5;
hole_offset = 20;
base_t=5;
housing_h = shaft_h+10;




module vslot_end_pivot (){


difference(){
    hull(){
        cuboid([40,40,base_t],rounding=1);

        up(shaft_h+wall)
        xcyl(d=shaft_d+(wall*2),h=40);

    }
    color("blue")
    up(shaft_h+(shaft_d/2))
        xcyl(d=shaft_d+shaft_clearence,h=50);
    color("green")
    ycopies(n=2,spacing=20)
    xcopies(n=2,spacing=20){
        zcyl(d=screw_d,h=housing_h*2.5);
        up(base_t/2)
        zcyl(d=screw_cap_d,h=housing_h,anchor=BOTTOM);

    }
}

}


vslot_end_pivot();
