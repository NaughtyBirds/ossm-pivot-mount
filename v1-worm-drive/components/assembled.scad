include <mount.scad>
include <settings.scad>
// $fn=72;
// starts=1;
// mod=2;
// gear_teeth=25;

$show_structure=true;
$show_drive=false;
$show_platten=false;

gear_shaft_d=8;
// worm_diam = 30;
// worm_length = 40;


if($show_structure){
fwd(gear_offset)
color("silver")xcyl(d=gear_shaft_d,h=100);

// work shaft
color("silver")
zcyl(d=gear_shaft_d,h=100);

//extrusion
back(worm_diam+gear_or+6)
color("silver")cuboid([40,150,40],anchor=FRONT);
}

if($show_drive){
    color("green")worm_drive(l=38,collar=0,modulus=mod,worm_starts=starts);
}
if($show_platten){
    fwd(gear_offset)color("purple")geared_mount(l=65,w=55,modulus=mod,gear_teeth=gear_teeth,worm_starts=starts,retention="nut",shaft_mount="bearing");
}

housing(mount_l=40,shaft_offset=gear_offset,show="all");


// up(40)back(motor_mount_l/2)
// motor_mount();
    // left(60)
    // geared_mount(l=65,w=55,modulus=1,worm_starts=starts);


// back(worm_diam/2)zrot(20){

// }



// // worm(mod=mod, d=30, l=50, starts=sstarts,$fn=72);

// $fn=36;


// right(worm_diam/2)
//   yrot($t*360/starts)
//     worm(d=worm_diam, l=worm_length, mod=mod, starts=starts, orient=BACK);
// left(pitch_radius(mod=mod, teeth=gear_teeth))
//   zrot(-$t*360/gear_teeth)
//     worm_gear(mod=mod, teeth=gear_teeth, worm_diam=worm_diam, worm_starts=starts);
