include <BOSL2/std.scad>
$fn=36;


module actuator_bracket_90_rear(
  l=60,
  t=3,
  w=40,
  wall1=5, // sides
  wall2=3,//
  h=70,
  pin_h=60,
  pin_d=6.2,
  pin_t=5,//minimum housing thicknes
  // pin_cap_slot_d=3,
  // pin_cap_slot_w=10,
  pin_xclearence=25,
  pin_yclearence=20,
  screw_d=5.25,
  screw_cap_d=10.25,
  screw_l=15,
  chamfer=0.5
  ) {
  base_l=l-pin_yclearence;
  side_w=(w-pin_xclearence)/2;
  pin_holder_l =pin_yclearence+pin_t;

  difference() {
    union(){
      cuboid([w,l,t],anchor=BOTTOM,chamfer=chamfer);
      fwd((l/2)-pin_holder_l)
      cuboid([w,wall2,h],anchor=BOTTOM+FRONT,chamfer=chamfer);


      xflip_copy(offset=w/2){
            fwd((l/2)-pin_holder_l )
            back(1)
            cuboid([side_w,pin_holder_l+1,h],anchor=BOTTOM+BACK+RIGHT,chamfer=chamfer);
        }
      xflip_copy(offset=(w/2)){
              hull(){
              // back(pin_yclearence/2)
              fwd((l/2)-pin_holder_l)
              cuboid([wall1,l-pin_holder_l,t],anchor=BOTTOM+FRONT+RIGHT,chamfer=chamfer);

              fwd(2+(l/2)-(pin_holder_l+wall2))
              cuboid([wall1,2,h],anchor=BOTTOM+FRONT+RIGHT,chamfer=chamfer);
              // ymove(l_offset)
              // up(h)
              // xcyl(d=,h=wall,chamfer=wall_chamfer);
              }
          }
    }

  fwd((l/2)-(pin_t+pin_d/2))
  zcopies(spacing=10,l=h-40,sp=30)
  xcyl(d=pin_d,l=w*2);

// xflip_copy(offset=0.5+w/2){
//             fwd((l/2)-(pin_t+pin_d/2))up(t)
//             cuboid([pin_cap_slot_d,pin_cap_slot_w,0+h-t],anchor=BOTTOM+RIGHT,chamfer=chamfer);
//         }

  screw_start = 0-pin_yclearence/2;
  ycopies(spacing = 12,l=l+screw_start-12,sp=screw_start)
  xcopies(spacing=20){
    down(1)
    zcyl(d=screw_d,h=h+50,anchor=BOTTOM);
    up(t)
    zcyl(d=screw_cap_d,h=screw_l+1,anchor=BOTTOM);

  }

  }

}


actuator_bracket_90_rear(t=8,l=60,h=80);
