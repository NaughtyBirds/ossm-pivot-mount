include <BOSL2/std.scad>
include <BOSL2/metric_screws.scad>
include <settings.scad>



module shaft_block(
    d=8,
    od=20,
    w=20,
    l=40,
    h=15,
    base_t=4,
    ex_slots=1,
    ex_slots_spacing=20,
    slot_w=5,
    bolt=M5,
    bolt_n=2,
    bolt_socket_d=10,
    bolt_spacing = 20,
    rounding=0,
    chamfer=0
) {

    difference() {
    hull(){
        cuboid(size=[w, l, base_t], anchor=BOTTOM,rounding=rounding,chamfer=chamfer,except=BOTTOM);
        up(h)xcyl(d=od,h=w,rounding=rounding,chamfer=chamfer);
    }

     up(h)xcyl(d=d+0.25,h=2*w);
    xcopies(n=ex_slots,spacing=ex_slots_spacing)
    ycopies(spacing=bolt_spacing,n=bolt_n){
        up(base_t) zcyl(d=bolt_socket_d,h=2*(h+od),anchor=BOTTOM);
        zcyl(d=bolt+0.5,h=3*(h+od));
    }
    }



}
// shaft_block(l=50,h=10,bolt_n=2,chamfer=1,base_t=3,bolt_spacing=30);
shaft_block(l=50,h=10,bolt_n=2,chamfer=1,base_t=3,bolt_spacing=30,w=40,ex_slots=2);
