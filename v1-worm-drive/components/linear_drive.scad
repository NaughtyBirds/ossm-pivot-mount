include <BOSL2/std.scad>
include <BOSL2/metric_screws.scad>
include <settings.scad>
module module_name(args) {
    // body...
}



module drive_mount_block(
    w=40,
    l=40,
    h=25,
    shaft_d = 6,
    shift=0,
    gap_w=20,
    base_t=4,
    ex_slots=1,
    ex_slots_spacing=20,
    slot_w=5,
    bolt=M5,
    bolt_n=2,
    bolt_socket_d=10,
    bolt_spacing = 20,
    rounding=1,
    chamfer=0
) {


top_od=20;
drive_shaft_d = gap_w;
gap_h = 15;

// tab_w = (w-drive_w)/2;
tab_w =w;

    difference() {
        hull(){
            cuboid(size=[tab_w, l, base_t], anchor=BOTTOM,rounding=rounding,chamfer=chamfer,except=BOTTOM);
            move([0,shift,h])
            // up(h)
            xcyl(d=top_od,h=tab_w,rounding=rounding,chamfer=chamfer);
        }

        up(h-gap_h)cuboid([gap_w,l*4,100],anchor=BOTTOM);
        move([0,shift,h])xcyl(d=shaft_d+0.25,h=w*3);
        xcopies(n=ex_slots,spacing=ex_slots_spacing)
        ycopies(spacing=bolt_spacing,n=bolt_n){
            up(base_t) zcyl(d=bolt_socket_d,h=100+h,anchor=BOTTOM);
            zcyl(d=bolt+0.5,h=100+h);
        }
    }
}

module drive_side_mount_block(
    w=40,
    l=40,
    h=25,
    shaft_d = 6,
    gap_w=20,
    base_t=4,
    ex_slots=1,
    ex_slots_spacing=20,
    slot_w=5,
    bolt=M5,
    bolt_n=2,
    bolt_socket_d=10,
    bolt_spacing = 20,
    rounding=1,
    chamfer=0
    ) {


    top_od=20;
    drive_shaft_d = gap_w;
    gap_h = 15;

    // tab_w = (w-drive_w)/2;
    tab_w =w;

    difference() {

        hull(){
            cuboid(size=[tab_w, l, base_t], anchor=BOTTOM,rounding=rounding,chamfer=chamfer,except=BOTTOM);
            up(h)
            xcyl(d=top_od,h=tab_w,rounding=rounding,chamfer=chamfer);
        }

        up(h-gap_h)cuboid([gap_w,l,100],anchor=BOTTOM);
        up(h)xcyl(d=shaft_d+0.25,h=w*3);
        xcopies(n=ex_slots,spacing=ex_slots_spacing)
        ycopies(spacing=bolt_spacing,n=bolt_n){
            up(base_t) zcyl(d=bolt_socket_d,h=100+h,anchor=BOTTOM);
            zcyl(d=bolt+0.5,h=100+h);
        }
    }
}



xdistribute(spacing=50){

// set to a little over 6.35 to use a 1/4 bolt
rod_d = 7;
// lengthwise 2040
drive_mount_block(ex_slots=2,l=50,bolt_spacing=22,shaft_d=rod_d,shift = -22);
// drive_side_mount_block(ex_slots=2,l=80,bolt_spacing=60);





// drive_mount_block(ex_slots=1,l=80,bolt_spacing=50,bolt=10,bolt_socket_d=25,base_t=10);

}
