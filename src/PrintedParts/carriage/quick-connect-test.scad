include <BOSL2/std.scad>
include <BOSL2/joiners.scad>

qc_slop=0.2;
qc_dovetail_w=20;
qc_dovetail_h=8;
qc_dovetail_slide=15;
qc_dovetail_slope=4; // 4,6,8




module test_fitting(
  dovetail_w=20,
  dovetail_h=8,
  slide=20,
  slope=4,
  base_h=5,
  chamfer=0,
  taper=0,
  // tapermm=1,
  $slop=0.1
) {
fbase_h = dovetail_h+base_h;
fbase_l=slide+5;
  xdistribute(spacing=(dovetail_w*1.5)){
    union(){
      cuboid([dovetail_w+5,slide+4,base_h],chamfer=chamfer,anchor=BOTTOM,edges="Z");
      up(base_h){
        // dovetail("male", width=dovetail_w, height=dovetail_h, slide=slide,r=0.5,slope=slope,round=false,);
        if (taper!=0) {
          dovetail("male", width=dovetail_w,taper=taper, height=dovetail_h, slide=slide,chamfer=0.5,slope=slope,round=false);
        }else{
          dovetail("male", width=dovetail_w,back_width=dovetail_w-1, height=dovetail_h, slide=slide,chamfer=0.5,slope=slope,round=false);
        }

      }
    }

    difference(){
      back((fbase_l-slide)/2)
      cuboid([dovetail_w+5,fbase_l,fbase_h],chamfer=chamfer,anchor=BOTTOM,edges="Z");
      up(fbase_h){
        if (taper!=0) {
          dovetail("female", width=dovetail_w,taper=taper, height=dovetail_h, slide=slide,slope=slope,chamfer=0.5,round=false);

        } else {
          dovetail("female", width=dovetail_w,back_width=dovetail_w-1, height=dovetail_h, slide=slide,slope=slope,chamfer=0.5,round=false);

        }
      }
    }
  }
}

ydistribute(spacing=qc_dovetail_slide*1.5){
  // test_fitting($slop=0.1,slide=10);
  test_fitting($slop=0.2,chamfer=2,slide=20,base_h=4,taper=4);

}
